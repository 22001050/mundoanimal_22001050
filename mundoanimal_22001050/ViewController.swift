//
//  ViewController.swift
//  mundoanimal_22001050
//
//  Created by COTEMIG on 17/11/22.
//

import UIKit
import Alamofire
import Kingfisher
import SwiftUI

struct Animal: Decodable {
    let name: String
    let latin_name: String
    let image_link: String
}

class ViewController: UIViewController {
    
    @IBOutlet weak var imageAnimal: UIImageView!
    @IBOutlet weak var nameAnimal: UINavigationItem!
    @IBOutlet weak var latinName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getAnimal()
    }
    
    func getAnimal() {
            AF.request("https://zoo-animal-api.herokuapp.com/animals/rand").responseDecodable(of: Animal.self){
                response in
                if let animal = response.value {
                    self.imageAnimal.kf.setImage(with: URL(string: animal.image_link))
                    self.latinName.text = animal.latin_name
                    self.nameAnimal.title = animal.name
                }
            }
        }
}

